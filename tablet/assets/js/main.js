document.addEventListener("contextmenu", function (e) {
  e.preventDefault();
}, false);
var numberofplayer = 1
var continuestatuscheck;
var url = "http://192.168.15.41/pmg_piano"
$(document).ready(function () {
  $("#add").click(function () {
    addnewRow(numberofplayer++)
  });
  $("#delete").click(function(event){
      // event.preventDefault();
      if(numberofplayer <2){
          return false;
      }
      $("#div"+numberofplayer).remove()
      numberofplayer = numberofplayer-1;
      // $("body").css('background','url("assets/img/gradient.png")')
      //return false;
  });
  $("#step1start").click(function(){
    $.ajax({
        type: "POST",
        url: url + '/server.php',
        data: {
          action: "_CHANGESTATUS",
          data: 2
        },
        success: function (response) {
          response = JSON.parse(response);
          if (response && response.response == "1") {
              $("#step1").hide()
              $("#step1end").show()
              $("#step2end").hide()
              $("#regis").hide();
              
          }
    
        },
    
        error: function (error) {}
      });
  });
  $("#step1endbtn").click(function(){
    $.ajax({
        type: "POST",
        url: url + '/server.php',
        data: {
          action: "_CHANGESTATUS",
          data: 3
        },
        success: function (response) {
          response = JSON.parse(response);
          if (response && response.response == "1") {
            $("#step1").hide()
              $("#step1end").hide()
              $("#step2end").show()
              $("#regis").hide()
          }
    
        },
    
        error: function (error) {}
      });
  });
  
  $("#step2endbtn").click(function(){
    $.ajax({
        type: "POST",
        url: url + '/server.php',
        data: {
          action: "_CHANGESTATUS",
          data: 4
        },
        success: function (response) {
          response = JSON.parse(response);
          if (response && response.response == "1") {
            $("#step1").hide()
              $("#step1end").hide()
              $("#regis").show();
              numberofplayer = 1;
              $("input").val("")
              $("#step2end").hide()
          }
    
        },
    
        error: function (error) {}
      });
  });
});


function addnewRow(np) {
  var html = '<div class="col-md-12 user" id="div'+numberofplayer+'">\
<div class="form-group">\
    <label for="email' + np + '">Email</label>\
    <input type="email" required class="form-control" placeholder="Enter email" id="email' + np + '">\
  </div>\
  <div class="form-group">\
    <label for="name' + np + '">Name</label>\
    <input type="text" required class="form-control" placeholder="Enter Name" id="name' + np + '">\
  </div>\
  <div class="form-group">\
    <label for="mobile' + np + '">Mobile Number</label>\
    <input type="text" inputmode="numeric" pattern="[0-9]*" required class="form-control" placeholder="Enter Mobile Number" id="mobile' + np + '">\
  </div>\
</div>'
  $("#forms").append(html)
}


function submitData() {
  var data = []
  for (let i = 1; i <= numberofplayer; i++) {
    data.push({
      name: $("#name" + i).val(),
      mobile: $("#mobile" + i).val(),
      email: $("#email" + i).val()
    })
  }

  $.ajax({
    type: "POST",
    url: url + '/server.php',
    data: {
      action: "_SAVE",
      data: data
    },
    success: function (response) {
      response = JSON.parse(response);
      if (response && response.response == "1") {
          $("#step1").show()
          
          $("#step2end").hide()
          $("#regis").hide();
          $("input").val("")
          checkStatus();
      }
      

    },

    error: function (error) {}
  });
}

function checkStatus() {
  $.ajax({
    type: "POST",
    url: url + '/server.php',
    data: {
      action: "READSTATUS",
    },
    success: function (response) {
      response = JSON.parse(response);
      
      if (response && response.response == "1") {
        $("#step1").show()
        $("#step1end").hide()
        $("#regis").hide();
        $("#step2end").hide()
      }if (response && response.response == "2") {
        $("#step1").hide()
        $("#step1end").show()
        $("#regis").hide();
        $("#step2end").hide()
      }
      if (response && response.response == "3") {
        $("#step1").hide()
        $("#step1end").hide()
        $("#regis").hide()
        $("#step2end").show()
        //clearTimeout(continuestatuscheck)
      }
      if (response && response.response == "4") {
        $("#step1").hide()
        $("#step1end").hide()
        $("#regis").show()
        $("#step2end").hide()
       // clearTimeout(continuestatuscheck)
      }
      continuestatuscheck = setTimeout(checkStatus, 200);

    },

    error: function (error) {}
  });
}
