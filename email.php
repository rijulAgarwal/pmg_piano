<?php
    error_reporting(E_ALL);
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 0);
    header("Access-Control-Allow-Origin: *");

    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;
    use PHPMailer\PHPMailer\POP3;
    //Load Composer's autoloader
    require __DIR__ . '/vendor/autoload.php';

    function send_email($to, $attachment, $body){
        // $pop = POP3::popBeforeSmtp('mail.pmgasia.in', 995, 30, 'maaza@pmgasia.in', '9311291593', 1);
        $mail = new PHPMailer(true);
        try {
            $mail->isSMTP();     
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );                                // Set mailer to use SMTP
            $mail->Host = 'mail.pmgasia.in';  // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = 'maaza@pmgasia.in';                 // SMTP username
            $mail->Password = '9311291593';                           // SMTP password
            $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 465;                                    // TCP port to connect to
        
            $mail->setFrom('maaza@pmgasia.in','Maaza');
            foreach($to as $e){
                $mail->addAddress($e);
            }
            
            
            $mail->addAttachment($attachment);    // Optional name
            $mail->isHTML(true);                                  // Set email format to HTML
        
            $mail->Subject = "MAAZA MELODY";
            $mail->Body    = $body;
            $mail->AltBody = "Hi";
        
            if(!$mail->send()) {
                header('Content-Type: application/json');
                $dataArray =array( 
                    "success" => false,
                    "message" => "Message could not be sent.",
                    "error" => "Mailer Error: " . $mail->ErrorInfo,
                );
                return $dataArray;
                //die(json_encode($dataArray));
            } else {
                header('Content-Type: application/json');
                $dataArray =array( 
                    "success" => true,
                    "message" => "Message has been sent.",
                    "error" => "Mailer Error: " . $mail->ErrorInfo,
                );
                return $dataArray;
                //die(json_encode($dataArray));
            }
        } catch (Exception $e) {
            echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
        }
    }

    // send_email("er.ashish888@gmail.com", "", "subject", "body", "assets/gif/37/1.jpeg");
?>