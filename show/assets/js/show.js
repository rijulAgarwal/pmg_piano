document.addEventListener("contextmenu", function (e) {
  e.preventDefault();
}, false);
var musicstring = ''
var isaudio = false
var delatInterval = false;
var isidle = true;
var correctnodes = 0;
var wrongnodes = 0;
var percent = 0;
// var mappingJson = {
//   A: "a4",
//   B: "B4",
//   C: "C4",
//   D: "C5",
//   E: "d4",
//   F: "d5",
//   G: "D4",
//   H: "g4",
//   I: "F4",
//   J: "G4",
//   K: "D5"
// }
// var mappingJson = {
//   D: "a4",
//   C: "B4",
//   F: "C4",
//   H: "C5",
//   I: "d4",
//   K: "d5",
//   L: "D4",
//   P: "g4",
//   O: "F4",
//   R: "G4",
//   Z: "D5"
// }
var mappingJson = {
  A: "C4",
  B: "d4",
  C: "G4",
  D: "a4",
  E: "F4",
  F: "B4",
  G: "d5",
  H: "C5",
  I: "g4",
  J: "D5",
  K: "D4",
  L: "C4",
  M: "d4",
  N: "G4",
  O: "a4",
  P: "F4",
  Q: "B4",
  R: "d5",
  S: "C5",
  T: "g4",
  U: "D5",
  V: "D4",
  W: "a4",
  X: "F4"

}
// A:"C4", B: G4,C:"F4",D:"d4",E:"C4",F
// var shownode = ("C4,C4,G4,F4,D4#,D4#,D4#,D4#,D4#,D4#,D4#,D4#,D4#,D4#,D4#,D4#,D4#,D4#,C4,C4,G4,F4,D4#,D4#,D4#,D4#,D4#,D4#,D4#,D4#,D4#,D4#,D4#,D4#,D4#,D4#,G4#,G4#,G4#,G4#,G4#,G4#,D5#,C5,A4#,G4#,G4#,G4,G4,G4,G4,G4,G4,G4,D5,G4,D5,G4,G4#,G4#,G4#,G4#,G4#,G4#,D5#,C5,A4#,G4#,G4#,G4,G4,G4,G4,G4,G4,D5,C5,B4,D5,C5,C4,C4,G4,F4,D4#,D4#,D4#,D4#,D4#,D4#,D4#,D4#,D4#,D4#,D4#,D4#,D4#,D4#").split(",")
//$.each(mappingJson,function(i,v){stt=stt.split(v+",").join(i+",")})
// var shownode = ("C,C,J,I,E,E,E,E,E,E,E,E,E,E,E,E,E,E,C,C,J,I,E,E,E,E,E,E,E,E,E,E,E,E,E,E,H,H,H,H,H,H,F,D,A,H,H,J,J,J,J,J,J,J,K,J,K,J,H,H,H,H,H,H,F,D,A,H,H,J,J,J,J,J,J,K,D,B,K,D,C,C,J,I,E,E,E,E,E,E,E,E,E,E,E,E,E,E").split(',')
// var shownode = ("F,F,R,O,I,I,I,I,I,I,I,I,I,I,I,I,I,I,F,F,R,O,I,I,I,I,I,I,I,I,I,I,I,I,I,I,P,P,P,P,P,P,K,H,D,P,P,R,R,R,R,R,R,R,K,R,K,R,P,P,P,P,P,P,K,H,D,P,P,R,R,R,R,R,R,K,H,C,K,H,F,F,R,O,I,I,I,I,I,I,I,I,I,I,I,I,I,I").split(',')
var shownode = ("A,A,C,E,B,B,B,B,B,B,B,B,B,B,B,B,B,B,A,A,C,E,B,B,B,B,B,B,B,B,B,B,B,B,B,B,I,I,I,I,I,I,G,H,D,I,I,C,C,C,C,C,C,C,J,C,J,C,I,I,I,I,I,I,G,H,D,I,I,C,C,C,C,C,C,J,H,F,J,H,A,A,C,E,B,B,B,B,B,B,B,B,B,B,B,B,B,B").split(',')
var url = "http://localhost/pmg_piano"
var shownodeindex = 0
var isplaying = false;
var isready = false;
var isplayoutput = false;
var counter = 5;
var tmpaudio;
var settimeIntervalNodes;
var counterInterval;
var issaved = false;
var video_player    = null;
var video_list      = ["assets/videos/rakul_tamil.mp4","assets/videos/aditi.mp4","assets/videos/rakul_telg.mp4"];
var video_index     = 0;

$(document).ready(function () {
  onload()
  setTimeout(checkStatus, 200);
  tmpaudio = document.createElement("audio")
  tmpaudio.src = "assets/music/bgaudiov3.mp3";

});

$(document).keydown(function (e) {
  var charCode = (e.which) ? e.which : e.keyCode;
  var char = String.fromCharCode(charCode);
  if (isidle) {
    return false;
  }
  if (isplayoutput) {
    $("#afterdone h1").text("PLAYING YOUR MELODY")
    var resultaudio = document.getElementById("outputSound")
    resultaudio.play()
    resultaudio.onended = function () {
      $.ajax({
        type: "POST",
        url: url + '/server.php',
        data: {
          action: "_CHANGESTATUS",
          data: 4
        },
        success: function (response) {
          response = JSON.parse(response);
          if (response && response.response == "1") { }

        },

        error: function (error) { }
      });
      $("#afterdone h1").text("THANK YOU FOR YOUR PARTICIPATION")
      // setTimeout(reset, 3000)
      reset()
    }
    return false;
  }
  if (!isready) {
    tmpaudio.currentTime = 0
    tmpaudio.play();
    $("#ready h1").text("Are you ready?");
    setTimeout(() => {
      counterInterval = setInterval(() => {
        if (counter < 1) {
          clearInterval(counterInterval);
          isplaying = true;
          shownodeindex = 0
          musicstring = "";
          $("#ad").hide();
          $("#ready").hide()
          $("#demo").show();
          callNewNode(char);
          if(settimeIntervalNodes){
            clearInterval(settimeIntervalNodes)
          }
          settimeIntervalNodes = setInterval(() => {
            callNewNode(char);
          }, 1000);
          return false;
        }
        $("#ready h1").text(counter--);
      }, 1000);
    }, 5000);
    isready = true;


  }
  if (!isplaying && isready) {
    return false;
  }
  tmpaudio.pause();
  if (!mappingJson[char]) {
    return false;
  }

  var nod = mappingJson[char]
  musicstring += nod + ","
  $("#tone").attr("src", "assets/music/" + nod + ".mp3")
  var audio = document.getElementById("tone")
  isaudio = true
  audio.play()
  if (delatInterval) {
    clearInterval(delatInterval)
    delatInterval = false;
  }
  // audio.onended = function () {
  //   //delatInterval = setInterval(addDelay, 100);
  //   //callNewNode()
  // }
})

function checkStatus() {
  $.ajax({
    type: "POST",
    url: url + '/server.php',
    data: {
      action: "READSTATUS",
    },
    success: function (response) {
      response = JSON.parse(response);
      if (response && response.response == "2") {
        if (!isplaying && !isready) {
          $("#ad").hide();
          $("#demo").hide();
          $("#afterdone").hide();
          $("#ready").show();
          isidle = false;
          $("#ready h1").text("Press any key to start");
        }

      } else if (response && response.response == "3") {
        if (!issaved) {
          savetone();
          issaved = true;
        }
      }
      else if (response && response.response == "4") {
        $("#afterdone h1").text("THANK YOU FOR YOUR PARTICIPATION")
        // setTimeout(reset, 3000)
        reset()
      }
      setTimeout(checkStatus, 200);

    },

    error: function (error) { }
  });
}

function addDelay() {
  if (!isaudio) {
    musicstring += "D" + ","
  }
}

function savetone() {
  if(settimeIntervalNodes){
    clearInterval(settimeIntervalNodes)
  }
  $.ajax({
    type: "POST",
    url: url + '/server.php',
    data: {
      action: "SAVEREC",
      data: musicstring
    },
    success: function (response) {

      response = JSON.parse(response);
      if (response && response.response == "1") {
        $("#afterdone h1").text("Press any key to hear the melody that you have created")
        $("#outputSound").attr("src", url + "/outputs/" + response.group_id + ".mp3");
        $("#demo").hide()
        $("#ready").hide()
        $("#ad").hide();
        $("#afterdone").show();
        isplaying = false;
        isready = false;
        isplayoutput = true;
        sendEmail(response.group_id)
      }
    },

    error: function (error) { }
  });
}


function callNewNode(char) {
  if (shownodeindex > shownode.length) {
    tmpaudio.pause();
    clearInterval(settimeIntervalNodes)
    $.ajax({
      type: "POST",
      url: url + '/server.php',
      data: {
        action: "_CHANGESTATUS",
        data: 3
      },
      success: function (response) {
        response = JSON.parse(response);
        if (response && response.response == "1") { }

      },

      error: function (error) { }
    });
    return false;
  }
  if (shownodeindex != 0) {
    if (char == shownode[shownodeindex - 1]) {
      $("#right p").text(++correctnodes)
    } else {
      $("#false p").text(++wrongnodes)
    }
    $("#percentage p").text(shownodeindex)
  }
  $("#beats").hide()
  $("#beats").text(shownode[shownodeindex++])
  $("#beats").fadeIn(200)
}

function sendEmail(group_id) {
  $.ajax({
    type: "POST",
    url: url + '/server.php',
    data: {
      action: "_SENDEMAIL",
      id: group_id
    },
    success: function (response) {


    },

    error: function (error) { }
  });
}

function reset() {
  shownodeindex = 0
  isplaying = false;
  isready = false;
  isplayoutput = false;
  counter = 5;
  isidle = true;
  issaved = false;
  settimeIntervalNodes=false;
  counterInterval=false;
  musicstring = ''
  $("#demo").hide()
  $("#ready").hide();
  $("#outputSound").attr("src", "");
  setTimeout(function () {
    $("#afterdone").hide();
    $("#ad").show();
  }, 3000)
}

function onVideoEnded(){
  if(video_index < video_list.length - 1){
      video_index++;
  }
  else{
      video_index = 0;
  }
  video_player.setAttribute("src", video_list[video_index]);
  video_player.play();
}

function onload(){
  video_player        = document.getElementById("ad");
  video_player.setAttribute("src", video_list[video_index]);
  video_player.play();
}
