-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 26, 2020 at 06:59 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pmg_piano`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_bin NOT NULL,
  `email` varchar(255) COLLATE utf8_bin NOT NULL,
  `mobile_number` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `group_id` int(11) NOT NULL,
  `is_audio_ready` int(11) NOT NULL DEFAULT 0,
  `is_email` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `mobile_number`, `group_id`, `is_audio_ready`, `is_email`) VALUES
(1, 'wwq', 'rijul.agarwal@tagbin.in', '9999999999', 28, 0, 1),
(2, 'Lkkhh', 'rijl@hh.com', '9999999999', 29, 0, 0),
(3, 'Rijul', 'rijul@gmail.com', '9777777777', 30, 0, 0),
(4, 'Rijj', 'hhhh@fff.com', '8888888888', 31, 0, 0),
(5, 'Rijul', 'rijul.agarwal@tagbin.in', '9999999999', 32, 0, 0),
(6, 'I Indra', 'indra.varma@tagbin.in', '8318523563', 33, 0, 1),
(7, 'Aneesh Gulati', 'aneesh.gulati@pmgasia.com', '9910431334', 34, 0, 1),
(8, 'Rijul', 'rijul@gmai.com', '', 35, 0, 0),
(9, 'rijul', 'rijul22@gmail.com', '', 36, 0, 0),
(10, 'Rijj', 'rijul.gc@ff.com', '', 37, 0, 1),
(11, 'Rii', 'rijul.agarwal@tagbin.in', '', 38, 0, 1),
(12, 'Ag', 'aneeshgulati28@gmail.com', '', 39, 0, 1),
(13, 'Rijul', 'rijl@hh.com', '', 40, 0, 0),
(14, 'Rijul', 'yogesh@tagbin.in', '', 41, 0, 0),
(15, 'Yogesh', 'yogesh@tagbin.in', '', 42, 0, 1),
(16, 'Yogehs', 'yogesh@tagbin.in', '', 43, 0, 0),
(17, 'Ag', 'aneesh.gulati@pmgasia.com', '9910431334', 44, 0, 1),
(18, 'Ag', 'aneesh.gulati@pmgaisa.com', '9910431334', 45, 0, 1),
(19, 'Rij', 'yogesh.kumar@tagbin.in', '', 46, 0, 1),
(20, 'Patrick james', 'patrickjames993@gmail.com', '9643420507', 47, 0, 1),
(21, 'Aneesh', 'aneesh.gulati@pmgasia.com', '9643420507', 48, 0, 1),
(22, 'Xyz', 'fnafees@coca.com', '', 49, 0, 1),
(23, 'Akash', 'therealakashyadav@gmail.com', '9820182954', 50, 0, 1),
(24, 'Priya', 'priyach.17oct@gmail.com', '9368943836', 51, 0, 0),
(25, 'NAT', 'ns@coca-cola.com', '8980433633', 52, 0, 1),
(26, 'Farzana', 'fnafees@coca-cola.com', '', 53, 0, 1),
(27, 'Priya', 'priyach.17oct@gmail.com', '', 54, 0, 1),
(28, 'Jeet', 'jeet@gmail.com', '8130553744', 55, 0, 1),
(29, 'Chirag suneja', 'chiragthemindful@gmail.com', '8130159520', 56, 0, 1),
(30, 'Atul', 'atulrks@hotmail.com', '', 57, 0, 1),
(31, 'Anil Sharma', 'info.fabworks@gmail.com', '', 58, 0, 0),
(32, 'Ankita', 'amahna@coca-cola.com', '', 59, 0, 0),
(33, 'Rijul', 'rijul.agarwal@tagbin.in', '', 60, 0, 1),
(34, 'Ashish', 'ashishmauryamzp1992@gmai.com', '8456461843', 61, 0, 1),
(35, 'Megha', 'mraturi@coca-cola.com', '', 62, 0, 1),
(36, 'Tejasvi', 'tejasvipradhan95@gmail.com', '', 63, 0, 1),
(37, 'Lakshay', 'lk@tagglabs.in', '7990012254', 64, 0, 1),
(38, 'Ankur', 'ankur.t@webcontxt.in', '7404040431', 65, 0, 1),
(39, 'Sheryl', 'sheryl.anth25@gmail.com', '9987194130', 66, 0, 1),
(40, 'Debashree', 'dgain@coca-cola.com', '8879153667', 67, 0, 1),
(41, 'Farzana', 'fnafees@coca-cola.com', '', 68, 0, 1),
(42, 'Nitin', 'nitin.gupta@tagbin.in', '', 69, 0, 1),
(43, 'Saloni', 'salkapoor@coca-cola.com', '', 70, 0, 1),
(44, 'Kritika arora', 'arorakritika4@gmail.com', '9873250477', 71, 0, 1),
(45, 'Sana khan', 'sonalikhansk91081@gmail.com', '', 72, 0, 1),
(46, 'Abhishek  singh', 'absingh@coca-cola.com.np', '', 73, 0, 1),
(47, 'Rajnish sharma', 'rsharma@coca-COLA.com', '', 74, 0, 1),
(48, 'Ashish khandelwal', 'ashishkhandelwal@cocacolabareilly.com', '9837083880', 75, 0, 1),
(49, 'ANKUR MITTAL', 'ankur.mittal@slmgbev.com', '7351055555', 76, 0, 1),
(50, 'Sanjeev sharma', 'sasharma1406@gmail.com', '', 77, 0, 1),
(51, 'Milind', 'milindderasari@gmail.com', '9998551331', 78, 0, 1),
(52, 'Rajnish', 'rajnishkapoor@lbpl.in', '', 79, 0, 1),
(53, 'Praveen Bhatia', 'bhatia@enrichagro.com', '7303602600', 80, 0, 1),
(54, 'Baljeet singh', 'baljeetsingh@enrichagro.com', '', 81, 0, 1),
(55, 'Mahesh', 'mahesh.kumar@enrichagro.com', '9650791220', 82, 0, 1),
(56, 'Mohiuddin Monem', 'mohiuddin@amlbd.com', '0171300797', 83, 0, 1),
(57, 'Sanjeev marya', 'suninair@coca-cola.com', '', 84, 0, 1),
(58, 'Deepak', 'fnafees@coca-cola.com', '', 85, 0, 1),
(59, 'Rekha', 'redatta@coca-cola.com', '', 86, 0, 1),
(60, 'Pihu', 'priyachaudhary17oct@gmail.com', '', 87, 0, 1),
(61, 'Jyoti simha', 'jysinha@coca-cola.in', '9818896494', 88, 0, 1),
(62, 'Akshay', 'akchopra@coca-Cola.in', '9873565583', 89, 0, 1),
(63, 'Shehnaz', 'gill@coca-cola.com', '', 90, 0, 0),
(64, 'Shehnaz', 'gill@coca-cola.com', '', 91, 0, 1),
(65, 'Sharda', 'vijaysharda@enrichagro.com', '', 92, 0, 1),
(66, 'Vikas', 'vsunkad@coca-cola.com', '', 93, 0, 1),
(67, 'Aakash', 'aakohli@coca-cola.com', '', 94, 0, 1),
(68, 'Kunal', 'ksaha@coca-cola.com', '', 95, 0, 1),
(69, 'Yogesh Kumar', 'ykumar9@coca-cola.com', '', 96, 0, 1),
(70, 'Ashish', 'ashish.maurya@tagbin.in', '7564587158', 97, 0, 1),
(71, 'Ashish', 'ashish.maurya@tagbin.in', '2345378698', 98, 0, 1),
(72, 'Ashish', 'ashish.maurya@tagbin.in', '', 99, 0, 1),
(73, 'Ds', 'dear@gmail.com', '1234567890', 100, 0, 1),
(74, 'Ashish', 'ashishmauryamzp1992@gmail.com', '', 101, 0, 1),
(75, 'Vani', 'vani.jain@coca-cola', '9958384447', 102, 0, 0),
(76, 'Ashish', 'ashishmauryamzp1992@gmail.com', '4646164648', 103, 0, 1),
(77, 'Priya', 'pihuch17oct@gmail.com', '', 104, 0, 1),
(78, 'SAMEER', 'sameerpathak@coca-cola.com', '9873440192', 105, 0, 1),
(79, 'Jejejdj', 'shhhs@hdh.com', '4646545565', 106, 0, 1),
(80, 'Priya', 'fnafees@coca-cola.com', '', 107, 0, 1),
(81, 'Kanika Sharma', 'kanika.sharma@coca-cola.com', '', 108, 0, 1),
(82, 'Farzana', 'fnafees@coca-cola.com', '', 109, 0, 1),
(83, 'Sjejdhdj', 'djejej@gmail.com', '6461646564', 110, 0, 1),
(84, 'Savio', 'savdsuza@coca-cola.com', '7838736839', 111, 0, 1),
(85, 'Tejasvi', 'tejasvipradhan95@gmail.com', '', 112, 0, 1),
(86, 'Rahul', 'raligade@cocacola.i', '', 113, 0, 1),
(87, 'Anindita', 'ANGUHA@COCA-COLA.COM', '', 114, 0, 1),
(88, 'N v m rao', 'navadu.venu@gmail.com', '9010526999', 115, 0, 1),
(89, 'Arun', 'arun.srivastav@slmgbev.com', '', 116, 0, 1),
(90, 'Esha', 'ekalramukhi@coca-cola.com', '', 117, 0, 1),
(91, 'Tejasvipradhan95@gmail.com ', 'tejasvipradhan95@gmail.com', '', 118, 0, 1),
(92, 'Shashi B Kumar', 'shashi.kumar@slmgbev.com', '9554049444', 119, 0, 1),
(93, 'Plallahabadi', 'plallahabadi@gmail.com', '', 120, 0, 1),
(94, 'Pihu', 'fnafees@coca-cola.com', '', 121, 0, 1),
(95, 'Pihu', 'fnafees@coca-cola.com', '', 121, 0, 1),
(96, 'narendra kumar', 'n_k_narang@yahoo.com', '', 122, 0, 1),
(97, 'Ram', 'ram@cocacolafzd.com', '9554953203', 123, 0, 1),
(98, 'Vijaya', 'vijayakumar.g@srisarvarayasugars.in', '', 124, 0, 1),
(99, 'Sujit', 'sujit@diamondbev.in', '', 125, 0, 1),
(100, 'Hhhh', 'fnafees@coca-cola.com', '', 126, 0, 0),
(101, 'G', 'fnafees@coca-cola.com', '', 127, 0, 1),
(102, 'Praveen', 'bhatia@enrichagro.com', '', 128, 0, 1),
(103, 'Sahil', 'sahil006090@gmail.com', '8475057362', 129, 0, 1),
(104, 'Ashish', 'ak268@hotmail.com', '', 130, 0, 1),
(105, 'Tejasb', 'fnafees@coca-cola.com', '', 131, 0, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
